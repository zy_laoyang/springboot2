<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>添加小说</title>
    <style type="text/css">
        html {
            font-family: sans-serif;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        body {
            margin: 10px;
        }
        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        td,th {
            padding: 0;
        }

        .pure-table {
            border-collapse: collapse;
            border-spacing: 0;
            empty-cells: show;
            border: 1px solid #cbcbcb;
        }

        .pure-table caption {
            color: #000;
            font: italic 85%/1 arial,sans-serif;
            padding: 1em 0;
            text-align: center;
        }

        .pure-table td,.pure-table th {
            border-left: 1px solid #cbcbcb;
            border-width: 0 0 0 1px;
            font-size: inherit;
            margin: 0;
            overflow: visible;
            padding: .5em 1em;
        }

        .pure-table thead {
            background-color: #e0e0e0;
            color: #000;
            text-align: left;
            vertical-align: bottom;
        }

        .pure-table td {
            background-color: transparent;
        }
    </style>
</head>
<body>
<h2>添加新小说</h2>
<br>
<form action="/add">
    小说名称<input type="text" name="novelname"/><br>
    小说作者<input type="text" name="author"/><br>
    小说类型<input type="text" name="type"/><br>
    小说状态<select name="status">
        <option value="1">连载中</option>
        <option value="2">完本</option>
    </select><br>
    简介：<textarea name="info"></textarea>
    <input type="submit">
</form>
</body>
</html>