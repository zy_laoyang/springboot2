<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>小说</title>
    <style type="text/css">
        html {
            font-family: sans-serif;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        body {
            margin: 10px;
        }
        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        td,th {
            padding: 0;
        }

        .pure-table {
            border-collapse: collapse;
            border-spacing: 0;
            empty-cells: show;
            border: 1px solid #cbcbcb;
        }

        .pure-table caption {
            color: #000;
            font: italic 85%/1 arial,sans-serif;
            padding: 1em 0;
            text-align: center;
        }

        .pure-table td,.pure-table th {
            border-left: 1px solid #cbcbcb;
            border-width: 0 0 0 1px;
            font-size: inherit;
            margin: 0;
            overflow: visible;
            padding: .5em 1em;
        }

        .pure-table thead {
            background-color: #e0e0e0;
            color: #000;
            text-align: left;
            vertical-align: bottom;
        }

        .pure-table td {
            background-color: transparent;
        }
    </style>
    <script>
        function pagego(type){
            var input_type=document.getElementById("noveltype");
            input_type.value=type;
            document.getElementById("pagefrm").submit();
        }

    </script>
</head>
<body>
 <h2>小说内容</h2>
 <br>
 <a href="/toadd">添加新小说</a>
 <a href="/addnovels">添加好多小说</a><br>
 类别：
 <form id="pagefrm" action="/" method="post">
     <input type="number" name="pageno" value="${page.pageno}">
     <input type="number" name="pagesize" value="${page.pagesiz!''}">
     <input type="hidden" name="type" id="noveltype" value="${novel.type!''}">
   小说名  <input type="text" name="novelname" id="novelname" value="${novel.novelname!''}">
     <input type="submit">
 </form>
 <nav> <#list types as type>
        <span onclick="pagego('${type}')">${type}</span>   &nbsp; &nbsp;
 </#list></nav>
 <span>共${page.pagecount} 页</span>
 当前第${page.pageno}页
 <table class="pure-table">
    <thead>
    <tr>
        <th width="10%">名称</th>
        <th width="10%">类型</th>
        <th width="10%">状态</th>
        <th width="50%">简介</th>
        <th width="10%">作者</th>
        <th width="10%">操作</th>
    </tr>
    </thead>
     <tbody>
     <#list page.list as novel>
        <tr>
            <td>${novel.novelname!''}</td>
            <td>${novel.type!''}</td>
            <td><#if novel.status==2>完本
                <#elseif novel.status==1>连载
                    <#else >未知
            </#if></td>
            <td>${novel.info!''}</td>
            <td>${novel.author!''}</td>
            <td><a href="/toupdate?nid=${novel.nid!''}">修改</a> |
                <a href="/del/${novel.nid!''}">删除</a></td>
        </tr>
     </#list>     
     </tbody>
 </table>
</body>
</html>