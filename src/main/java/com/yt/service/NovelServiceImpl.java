package com.yt.service;

import com.yt.entity.Novel;
import com.yt.entity.Page;
import com.yt.maper.NovelMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class NovelServiceImpl implements NovelService {

    @Resource
    NovelMapper novelMapper;

    @Override
    public List<Novel> getAll() {
        return novelMapper.getAllNovel();
    }

    @Override
    public Novel getNovelById(Integer nid) {

        return novelMapper.getNovelById(nid);
    }

    @Override
    public Integer update(Novel novel) {
        return novelMapper.updateNover(novel);
    }

    @Override
    public Integer add(Novel novel) {
        return novelMapper.addNovel(novel);
    }

    @Override
    public Integer del(Integer nid) {
        return novelMapper.delNover(nid);
    }

    @Override
    public Page<Novel> getPage(int pagesize, int pageno) {
        Page<Novel> page = new Page<>();
        page.setPageno(pageno);
        page.setPagesiz(pagesize);
        String condition = "1=1";
        page.setCount(novelMapper.getCount(condition).intValue());
        page.setPagecount(page.getCount() % pagesize == 0 ? page.getCount() / pagesize
                : page.getCount() / pagesize + 1);

        page.setList(novelMapper.getPage(condition, pagesize * (pageno - 1), pagesize));
        return page;
    }

    @Override
    public List<String> getTypes() {
        return novelMapper.getTypes();
    }

    @Override
    public Page<Novel> getPageByNovel(int pagesize, int pageno, Novel novel) {
        Page<Novel> page = new Page<>();
        page.setPageno(pageno);
        page.setPagesiz(pagesize);
        String condition = "1=1";


        if(novel.getType()!=null && !novel.getType().equals("")){
            condition+=" and type='"+novel.getType()+"' ";
        }


        if(novel.getNovelname()!=null && !novel.getNovelname().equals("")){
            condition+=" and novelname like '%"+novel.getNovelname()+"%' ";
        }
        page.setCount(novelMapper.getCount(condition).intValue());
        page.setPagecount(page.getCount() % pagesize == 0 ? page.getCount() / pagesize
                : page.getCount() / pagesize + 1);

        page.setList(novelMapper.getPage(condition, pagesize * (pageno - 1), pagesize));
        return page;
    }


}
