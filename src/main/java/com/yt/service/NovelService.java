package com.yt.service;

import com.yt.entity.Novel;
import com.yt.entity.Page;

import java.util.List;

public interface NovelService {
    List<Novel> getAll();
    Novel getNovelById(Integer nid);
    Integer update(Novel novel);
    Integer add(Novel novel);
    Integer del(Integer nid);

    Page<Novel> getPage(int pagesize, int pageno);

    List<String> getTypes();

    Page<Novel> getPageByNovel(int pagesize, int pageno, Novel novel);
}
