package com.yt.service;

import com.yt.entity.User;
import com.yt.maper.UserMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * RestController是Rest风格，http协议 返回 json 格式数据
 * 默认此类中所有方法前都有@ResponseBody
 */
@RestController
@Slf4j
public class HelloWorldService {

    @Autowired
    UserMapper userMapper;
    @RequestMapping("/getUser")
    public List<User> getUsers(){
        return userMapper.getUser();
    }

    @RequestMapping("/addUser")
    public String addUser(){
        userMapper.addUser("springboot","bootpwd");
        return "ok";
    }

    /**
     * Value注解用于读取本地配置文件
     * 第一次尝试出错。。。把()改用{}
     */
    @Value("${myuser.username}")
    String uname;
    @Value("${myuser.age}")
    int age;

    @RequestMapping("/getname")
    public String getName() {
        return "{\"NAME\":\"" + uname + "\"}";
    }

    @RequestMapping("/getage")
    public String getAge() {
        log.info(uname+age);
        return "age:" + age;
    }
}
