package com.yt;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Hello world!
 * 启动后报错，可能是没有扫描相关包
 *
 * @ComponentScan("com.yt") 扫描所有子包
 * 修改后启动正常，返回值 正常
 */
//@EnableAutoConfiguration
//@ComponentScan("com.yt")
//因为找不到controller 决定换成下面
@SpringBootApplication
//依然找不到controller头痛。
@EnableScheduling//启用定时任务
@MapperScan("com.yt.maper")//指定mybatis mapper文件位置
public class App {
    public static void main(String[] args) {
        //刚建立项目，测试git
        SpringApplication.run(App.class, args);
    }
}
