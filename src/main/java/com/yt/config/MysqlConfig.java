package com.yt.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Classname MysqlConfig
 * @Date 2021/10/16 16:48
 * @Create by 杨涛
 */
@Component
//需要加入自动扫描的注解
@ConfigurationProperties(prefix="mysqlconfig")
@Data
public class MysqlConfig {
    private String name;
}
