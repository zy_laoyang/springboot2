package com.yt.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Classname TaskTest
 * @Descriptions  定时任务，spring 自带功能
 * @Date 2021/10/16 18:49
 * @Create by 杨涛
 */
@Component
@Slf4j
public class TaskTest {
    //需要在启动类开启定时任务功能
//    @Scheduled(fixedRate = 3000)
    //使用表达式
    //可以使用生成器 https://www.bejson.com/othertools/cron/
//    @Scheduled(cron="5/3 * * * * ? ")
    public void taskshow(){

//        log.info("show me !"+System.currentTimeMillis()      );
    }

}
