package com.yt.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Classname UserConfig
 * @Date 2021/10/16 16:54
 * @Create by 杨涛
 */
@Component
@ConfigurationProperties(prefix="myuser")
@Data
public class UserConfig {
    private String username;
    private String age;
}
