package com.yt.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Classname TController
 * @Description TODO:s
 * @Date 2021/10/16 10:00
 * @Create by 杨涛
 */
//@RestController

/**
 * 测试后，新的controller 可以访问，但是需要设置responsebody
 * 在此方法后，修改一下
 */
@Controller
public class TController {
    @RequestMapping("/tc")
//    @ResponseBody
    public String getTc(Map<String,String> result){
        result.put("resultkey","resultvalue111");
        return "findex";
    }
}
