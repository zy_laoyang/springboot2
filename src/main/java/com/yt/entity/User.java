package com.yt.entity;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import lombok.extern.slf4j.XSlf4j;

/**
 * @Classname User
 * @Description TODO:
 * @Date 2021/10/16 16:18
 * @Create by 杨涛
 */
@Data//生成属性的getter setter
@Slf4j//注入log日志对象
public class User {
    private String username;
    private String password;

    public static void main(String[] args) {
        User u =new User();
        log.info("aa");
        //lombok可以用了。
//        u.setUsername();
    }
}
