package com.yt.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Page<T>{
    private Integer pagesiz=10;
    private Integer pageno=1;
    private Integer count;
    private Integer pagecount;
    List<T> list;
}
