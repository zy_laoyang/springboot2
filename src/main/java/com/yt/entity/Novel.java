package com.yt.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data//生成getter setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Novel {
    //名称，作者，分类，状态，简介，id(自已用来确认唯一的)
    private Integer nid;
    private String novelname;
    private Integer aid;
    private String type;
    private Integer status;
    private String info;
    private String author;
}
