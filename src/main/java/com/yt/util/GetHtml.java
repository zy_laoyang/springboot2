package com.yt.util;

import com.yt.entity.Author;
import com.yt.entity.Novel;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GetHtml {

    public List<Novel> getNovels(String url){

        List<Novel> novels=new ArrayList<>();
//        String url="http://www.zongheng.com/rank/details.html?rt=1&d=1&i=2&p=2";
        Connection con= Jsoup.connect(url);
        try {
            Document document = con.get();
//            System.out.println(document);
            String selector="div.rank_d_book_intro";
            Elements elements = document.select(selector);
            for (Element element:elements) {
                /// System.out.println(element.html());
                Novel novel=new Novel();
                //名称
                selector="div.rank_d_b_name>a";
                novel.setNovelname(element.select(selector).text());
                //作者
                selector="div.rank_d_b_cate>a";
                String [] authorandtype=element.select(selector).text().split(" ");


                //novel.setAuthor(authorandtype[0]);
                Author author=new Author(0,authorandtype[0]);

                novel.setAid(author.getId());
                novel.setType(authorandtype[1]);
                switch (authorandtype[2]){
                    case "连载":novel.setStatus(1);break;
                    case "完本":novel.setStatus(2);break;
                    default:novel.setStatus(0);
                }
                selector="div.rank_d_b_info";
                novel.setInfo(element.select(selector).text());

//                System.out.println(novel);


//                System.out.println("=================================================");
                novels.add(novel);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return novels;
    }
}
