package com.yt;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @Classname AopLog
 * @Description TODO:
 * @Date 2021/10/16 18:34
 * @Create by 杨涛
 */
@Aspect
@Component
@Slf4j
public class AopLog {
    @Pointcut("execution(public * com.yt..*.*(..))")
    public void weblog(){}
    @Before("weblog()")
    public void dobeforlog() {
        log.info("-----------------------------");
    }

}
