package com.yt.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * @Classname FreeMarkerIndexController
 * @Description TODO:试用 freemarker
 * @Date 2021/10/16 9:29
 * @Create by 杨涛
 */
@Controller
public class FreeMarkerIndexController {
    @RequestMapping("/freemarkerindex")
    public String getIndex(Map<String, String> result) {

        result.put("resultkey", "freemarker");
        return "findex";
    }
}
