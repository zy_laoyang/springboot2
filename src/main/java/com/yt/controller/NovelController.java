package com.yt.controller;

import com.yt.entity.Author;
import com.yt.entity.Novel;
import com.yt.entity.Page;
import com.yt.maper.AuthorMapper;
import com.yt.maper.NovelMapper;
import com.yt.service.NovelService;
import com.yt.util.GetHtml;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@Slf4j
public class NovelController {

    @Resource
    NovelService novelService;

    @Autowired
    AuthorMapper authorMapper;

    @RequestMapping("/addnovels")
    public String addnovels(){

//        GetHtml gh=new GetHtml();
        for(int i=1;i<=10;i++){
            String url="http://www.zongheng.com/rank/details.html?rt=1&d=1&i=2&p="+i;
            getHtml(url);
        }
        return "findex";
    }

    public void getHtml(String url){
        List<Novel> novels=new ArrayList<>();
//        String url="http://www.zongheng.com/rank/details.html?rt=1&d=1&i=2&p=2";
        Connection con= Jsoup.connect(url);
        try {
            Document document = con.get();
//            System.out.println(document);
            String selector="div.rank_d_book_intro";
            Elements elements = document.select(selector);
            for (Element element:elements) {
                /// System.out.println(element.html());
                Novel novel=new Novel();
                //名称
                selector="div.rank_d_b_name>a";
                novel.setNovelname(element.select(selector).text());
                //作者
                selector="div.rank_d_b_cate>a";
                String [] authorandtype=element.select(selector).text().split(" ");


                //novel.setAuthor(authorandtype[0]);
                Author author=new Author(0,authorandtype[0]);
                authorMapper.addAutor(author);
                novel.setAid(author.getId());
                novel.setType(authorandtype[1]);
                switch (authorandtype[2]){
                    case "连载":novel.setStatus(1);break;
                    case "完本":novel.setStatus(2);break;
                    default:novel.setStatus(0);
                }
                selector="div.rank_d_b_info";
                novel.setInfo(element.select(selector).text());

                log.info(novel.toString());
                novelService.add(novel);
//                System.out.println("=================================================");
                novels.add(novel);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("/")
    public String getAll(Map<String,Object> map,
                         @RequestParam(value="pagesize",defaultValue="10",required=false) int pagesize,
         @RequestParam(value="pageno",defaultValue="1",required=false) int pageno,Novel novel
    ){

        Page<Novel> page=null;
        log.info(novel.toString());
        map.put("novel",novel);
        if(novel!=null){
            if(novel.getType()!=null || novel.getNovelname()!=null){
                log.info("得到相应类型");
                page=novelService.getPageByNovel(pagesize,pageno,novel);
            }else{
                page=novelService.getPage(pagesize,pageno);
            }

        }else{
            page=novelService.getPage(pagesize,pageno);
        }


//      =novelService.getPage(pagesize,pageno);
        map.put("page",page);


        List<String> types=novelService.getTypes();

        map.put("types",types);
        return "findex";
    }
    @RequestMapping("/toadd")
    public String toadd(){
        return "addnovel";
    }


    @RequestMapping("/toupdate")
    public String toupdate(Integer nid,Map<String,Object> map){
        map.put("novel",novelService.getNovelById(nid));
        return "updatenovel";
    }

    @RequestMapping("/add")
    public String add(Novel novel){
        log.info("执行添加"+novel);

        novelService.add(novel);
        return "redirect:/";
    }

    @RequestMapping("/update")
    public String update(Novel novel){
        log.info("执行更新"+novel);
        novelService.update(novel);
        return "redirect:/";}


    @RequestMapping("/del/{nid}")
    public String del(@PathVariable("nid") int nid){

        log.info("执行删除"+nid);
        novelService.del(nid);

        return "redirect:/";}
}
