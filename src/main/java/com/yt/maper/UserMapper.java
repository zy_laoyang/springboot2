package com.yt.maper;

import com.yt.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Positive;
import java.util.List;

/**
 * @Classname BookMapper
 * @Description
 * @Date 2021/10/16 19:28
 * @Create by 杨涛
 */
@Component
public interface UserMapper {

    @Insert("INSERT INTO USER(username,password,nickname,regdate,level) values (#{username},#{password},'',NOW(),1)")
    void addUser(@Param("username")String username,@Param("password")String password);

    @Select("select * from user")
    List<User> getUser();
}
