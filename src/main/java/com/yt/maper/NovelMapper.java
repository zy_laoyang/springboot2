package com.yt.maper;

import com.yt.entity.Novel;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface NovelMapper {
    @Select("select * from novel n join author a on a.id =n.aid ")
    List<Novel> getAllNovel();

    @Select("select * from novel n join author a on a.id =n.aid  where n.nid = #{nid}")
    Novel getNovelById(Integer nid);
    @Select("select count(*) from novel where 1=1 and ${condition}")
    Long getCount(@Param("condition") String condition);


    @Select("select * from novel  n join author a on a.id =n.aid  where 1=1 and ${condition} limit #{start},#{end}")
    List<Novel> getPage(String condition,Integer start,Integer end);

    @Insert("INSERT INTO  novel VALUES (null,#{novelname},#{type}, #{status}, #{info}, #{aid} ) ")
    Integer addNovel(Novel novel);

    @Update("update novel set novelname=#{novelname} ,type=#{type} ,status=#{status} ,info=#{info} ,aid=#{aid} where nid =#{nid}")
    Integer updateNover(Novel novel);
    @Delete("delete from novel where nid =#{nid}")
    Integer delNover(Integer nid);


    @Select("SELECT DISTINCT TYPE FROM novel")
    List<String> getTypes();


}
