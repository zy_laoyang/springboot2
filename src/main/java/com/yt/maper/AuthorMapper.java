package com.yt.maper;

import com.yt.entity.Author;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.springframework.stereotype.Component;

@Component
public interface AuthorMapper {
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    @Insert("insert author(author) values(#{author})")
    public Integer addAutor(Author author);
}
