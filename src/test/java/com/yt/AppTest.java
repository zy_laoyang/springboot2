package com.yt;

import static org.junit.Assert.assertTrue;

import com.yt.entity.Novel;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;


import javax.net.ssl.SSLSocketFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.net.URL;
import java.util.Collection;
import java.util.Map;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()    {
        String url="http://www.zongheng.com/rank/details.html?rt=1&d=1&i=2&p=2";
        Connection con= Jsoup.connect(url);
        try {
            Document document = con.get();
//            System.out.println(document);
            String selector="div.rank_d_book_intro";
            Elements elements = document.select(selector);
            for (Element element:elements) {
               /// System.out.println(element.html());
                Novel novel=new Novel();
                //名称
                selector="div.rank_d_b_name>a";
                novel.setNovelname(element.select(selector).text());
                //作者
                selector="div.rank_d_b_cate>a";
                String [] authorandtype=element.select(selector).text().split(" ");
//                novel.setAuthor(authorandtype[0]);
                novel.setType(authorandtype[1]);
                switch (authorandtype[2]){
                    case "连载":novel.setStatus(1);break;
                    case "完本":novel.setStatus(2);break;
                    default:novel.setStatus(0);
                }
                selector="div.rank_d_b_info";
                novel.setInfo(element.select(selector).text());

                System.out.println(novel);


                System.out.println("=================================================");

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
